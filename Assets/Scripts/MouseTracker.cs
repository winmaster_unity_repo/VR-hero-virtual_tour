﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTracker : MonoBehaviour
{
	public float rotationSpeed= 5.0f;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
	{
		float mouseX = Input.mousePosition.x * rotationSpeed;
		float mouseY = Input.mousePosition.y * rotationSpeed;

		Debug.Log ("PositionX: " + mouseX + "\tPositionY: " + mouseY);
		Camera.main.transform.rotation = Quaternion.Euler (-mouseY, mouseX, 0);

	}
}